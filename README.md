# APIROMOS SDK
  
Paquete (SDK) para consumir el api de promociones con modelos.

  - Basado en modelos
  - Para php puro o Laravel

### Denpendicias

* "league/oauth2-client": "^2.2",
* "guzzlehttp/guzzle": "^6.3",
* "illuminate/support": "^5.4"

### Installation

Via Composer

``` bash
$ composer require apipromos/sdk
```