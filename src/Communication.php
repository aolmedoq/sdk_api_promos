<?php

namespace Apipromos\Sdk;

use Illuminate\Pagination\LengthAwarePaginator;

/**
 *
 */
class Communication extends AbstractModel
{   
    protected $dates = [ 
        'created_at',
        'updated_at',
    ];

    public static function find($id)
    {
        $result = api()->get('communications/' . $id);

        if ($result->getStatusCode() == 200) {
            $model = new self;
            $model->fill((array) getApiDataResponse($result));
            return $model;
        }

        return null;
    }

    public static function create($attributes = [])
    {
        $model = new self;
        $model->fill((array) $attributes);

        $result = api()->post('communications/', $model->getAttributes());

        if ($result->getStatusCode() == 201) {
            return $model;
        }

        return null;
    }

    public function updatePartials($attributes = [])
    {

        $result = api()->patch('communications/' . $this->id, $this->getAttributes());

        if ($result->getStatusCode() == 200) {
            $model = new self;
            $model->fill((array) getApiDataResponse($result));
            return $model;
        }

        return false;
    }

    public function update($attributes = [])
    {
        $result = api()->put('communications/' . $this->id, $this->getAttributes());

        if ($result->getStatusCode() == 200) {
            $model = new self;
            $model->fill((array) getApiDataResponse($result));
            return $model;
        }

        return false;
    }

    public function delete()
    {
        $result = api()->delete('communications/' . $this->id);

        if ($result->getStatusCode() == 200) {

            return $model;
        }

        return false;
    }

    public static function paginate($params = [], $config = [])
    {

        $collection = collect();

        $result = api()->get('communications/', $params);

        if ($result->getStatusCode() == 200) {

            $data       = getApiDataResponse($result);
            $pagination = getApiMetaPaginationResponse($result);

            foreach ($data as $value) {
                $model = new self;
                $model->fill($value);
                $collection->push($model);
            }

            return new LengthAwarePaginator(
                $collection,
                $pagination['total'],
                $pagination['per_page'],
                $pagination['current_page'],
                [
                    'path'  => isset($config['path']) ? $config['path'] : request()->url(),
                    'query' => isset($config['query']) ? $config['query'] : request()->query(),
                ]
            );
        }

        return new LengthAwarePaginator($collection, 0, 1, 1);
    }

}
