<?php
/**
 * ApiPromosClient
 */

namespace Apipromos\Sdk;

use Apipromos\Sdk\Exceptions\InvalidClientRequest;
use Apipromos\Sdk\TraitFileToken;
use GuzzleHttp\Client;

class ApiPromosClient
{
    use TraitFileToken;

    protected $client;

    protected $response;

    public function __construct($config = null)
    {
        $this->initClient($config);
    }

    public function initClient($config = null)
    {
        $this->client = new Client([
            'base_uri'    => $this->getApiBaseUri(),
            'headers'     =>
            [
                'Authorization' => "Bearer " . $this->getToken(), //. env('APIPROMOS_API_TOKEN'),
                'content-type'  => 'application/json',
                'Content-Language' => 'es'
            ],
            'http_errors' => $this->getHttpErrors(),
        ]);
    }

    public function getToken()
    {

        if ($this->readToken()) {
            return $this->readToken();
        }

        return $this->generateToken();

    }

    public function generateToken()
    {
        $guzzle = new Client;

        $this->response = $guzzle->post($this->getUrlAccessToken(), [
            'form_params' => [
                'grant_type'    => 'client_credentials',
                'client_id'     => $this->getClientId(),
                'client_secret' => $this->getClientSecret(),
            ],
            'http_errors' => false,
        ]);

        if ($this->response->getStatusCode() == 200) {
            $token = json_decode((string) $this->response->getBody(), true)['access_token'];
            $this->saveToken($token);
        } else { 
             throw new InvalidClientRequest("Error Processing Request API PROMOS", 0, null, $this->response);
        }

        return $token;
    }

    public static function setApiBaseUri($value)
    {
        return $GLOBALS['APIPROMOS_API_BASE_URI'] = $value;
    }

    public function getApiBaseUri()
    {
        return $GLOBALS['APIPROMOS_API_BASE_URI'];
    }

    public static function setHttpErrors($value)
    {
        return $GLOBALS['APIPROMOS_HTTP_ERRORS'] = $value;
    }
    public function getHttpErrors()
    {
        return false;
    }

    public static function setClientId($value)
    {
        return $GLOBALS['APIPROMOS_CLIENT_ID'] = $value;
    }

    public function getClientId()
    {
        return $GLOBALS['APIPROMOS_CLIENT_ID'];
    }

    public static function setClientSecret($value)
    {
        return $GLOBALS['APIPROMOS_CLIENT_SECRET'] = $value;
    }

    public function getClientSecret()
    {
        return $GLOBALS['APIPROMOS_CLIENT_SECRET'];
    }

    public static function setUrlAccessToken($value)
    {
        return $GLOBALS['APIPROMOS_URL_ACCESS_TOKEN'] = $value;
    }

    public function getUrlAccessToken()
    {
        return $GLOBALS['APIPROMOS_URL_ACCESS_TOKEN'];
    }

    public function post($uri, $params = [])
    {
        try {
            $this->response = $this->client->request('POST', $uri, [
                'json' => $params,
            ]);
            $this->middlewareTerminateRequest($uri, $params, __FUNCTION__);
            return $this->response;
        } catch (Exception $e) {

        }

    }

    public function get($uri, $params = [])
    {
        try {

            $this->response = $this->client->request('GET', $uri, [
                'query' => $params,
            ]);
            $this->middlewareTerminateRequest($uri, $params, __FUNCTION__);
            return $this->response;
        } catch (Exception $e) {

        }
    }

    public function put($uri, $params = [])
    {
        try {
            $this->response = $this->client->request('PUT', $uri, [
                'json' => $params,
            ]);
            $this->middlewareTerminateRequest($uri, $params, __FUNCTION__);
            return $this->response;
        } catch (Exception $e) {

        }
    }

    public function patch($uri, $params = [])
    {
        try {
            $this->response = $this->client->request('PATCH', $uri, [
                'json' => $params,
            ]);
            $this->middlewareTerminateRequest($uri, $params, __FUNCTION__);
            return $this->response;
        } catch (Exception $e) {

        }
    }

    public function delete($uri)
    {
        try {
            $this->response = $this->client->request('DELETE', $uri);
            $this->middlewareTerminateRequest($uri, $params, __FUNCTION__);
            return $this->response;
        } catch (Exception $e) {

        }
    } 

    public function middlewareTerminateRequest($uri, $params, $method)
    {

        if ($this->response->getStatusCode() == 401) {

            $this->removeToken();
            $this->initClient();

            $this->$method($uri, $params);

        }

        if ($this->response->getStatusCode() > 400) {
            throw new InvalidClientRequest("Error Processing Request API PROMOS", 0, null, $this->response);
        }

    }
}
